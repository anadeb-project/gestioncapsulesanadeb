from django.shortcuts import redirect

def home(request):
    """Fonction redirigeant l'utilisateur vers la liste d'affichage des capsules"""

    return redirect('capsule_liste')