from django.db import models
from capsules.librairies.models import UnixTimestampField

# Create your models here.

STATUS = (
    ('unknown', 'unknown'),
    ('active', 'active'),
    ('retired', 'retired'),
    ('destroyed', 'destroyed')
) # Représentation des STATUS des capsules

TYPE = (
    ('Dragon 1.0', 'Dragon 1.0'),
    ('Dragon 1.1', 'Dragon 1.1'),
    ('Dragon 2.0', 'Dragon 2.0')
) # Représentation des TYPEs des capsules


class Capsule(models.Model):
    """Model représentant la Capsule"""
    capsule_serial = models.CharField(max_length=100, verbose_name="Serial ", null=False, blank=False, unique=True)
    capsule_id = models.CharField(max_length=100, verbose_name="ID Capsule ", null=False, blank=False)
    status = models.CharField(choices=STATUS, max_length=15, verbose_name="Status ", null=False, blank=False, default='active')
    original_launch = models.DateTimeField(null=True)
    original_launch_unix = UnixTimestampField(null=True)
    landings = models.IntegerField(verbose_name="Water landings ", default=0)
    type = models.CharField(choices=TYPE, max_length=15, verbose_name="Type ", null=False, blank=False, default='Dragon 1.0')
    reuse_count = models.IntegerField(verbose_name="Reuse count ", default=0)
    details = models.TextField(verbose_name="Details ", blank=True, null=True)

    def missions(self):
        return self.mission_set.get_queryset()


class Mission(models.Model):
    """Model représentant la Mission"""
    capsule = models.ForeignKey(Capsule, verbose_name="Capsule ", on_delete = models.CASCADE, null=False)
    name = models.CharField(max_length=100, verbose_name="Name ", null=False, blank=False, unique=True)
    flight = models.IntegerField(verbose_name="Flight ", default=0)