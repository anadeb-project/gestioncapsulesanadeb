from django.shortcuts import render
from capsules.spacex_api.requests import get_all_capsules
from capsules.spacex_api.db_asynchrone import save_liste_capsules
from .models import Capsule

# Create your views here.


def liste_capsules(request):
    """Fonction permettant d'afficher la liste des capsules"""

    save_liste_capsules(get_all_capsules()) # Enregistrement des capsules récuperées sur SpaceX

    return render(request, 'capsules/liste.html', {
        'capsules' : Capsule.objects.all() # Récuperation de toutes les capsules dans la base de données
    })

def detail_capsules(request, capsule_serial: str):
    """Fonction permettant d'afficher le détail d'une capsule par sa série"""

    try:
        capsule = Capsule.objects.get(capsule_serial=capsule_serial) # Recuperation de la capsule dans la base de données
    except Exception as exc:
        capsule = None # Initiation de la capsule en nulle s'il n'existe pas une capsule ayant la série passée en parametre

    return render(request, 'capsules/detail.html', {
        'capsule' : capsule, 
        'capsule_serial' : capsule_serial,
        'missions' : capsule.missions() if capsule else [] # Récuperer la liste des missions d'une capsule si cette dernière existe, sinon retourner une liste vide
    })