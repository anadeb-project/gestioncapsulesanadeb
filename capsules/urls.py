from django.urls import path
from .views import *

# Les Urls des pages des capsules
urlpatterns = [
    path('liste/', liste_capsules, name="capsule_liste"),
    path('<str:capsule_serial>/detail/', detail_capsules, name="capsule_deatil"),
]
